FROM ubuntu:latest

# set work directory
WORKDIR /home/gutstickets

# install dependencies
RUN apt-get update
RUN apt-get install -y python3
RUN apt-get install -y python3-dev
RUN apt-get install -y python3-distutils
RUN apt-get install -y python3-pip
RUN apt-get install -y gunicorn
RUN apt-get install -y libpq-dev
RUN pip3 install --upgrade pip

COPY ./req .

RUN pip3 install -r req

COPY ./ .
COPY ./scripts/ .

RUN chmod 777 start.sh

ENTRYPOINT '/home/gutstickets/start.sh'
EXPOSE 8003
