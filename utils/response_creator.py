from rest_framework.response import Response


def create_response_schema(
        success: bool,
        status: int,
        data: dict = {}):
    return Response(data={
        "data": data,
        "success": success,
    }, status=status)
