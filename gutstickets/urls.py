from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from theater.urls import urlpatterns as theater_urlpatterns
from shop.urls import urlpatterns as shop_urlpatterns
from django.http.response import HttpResponse
from threading import Thread, get_ident, active_count, enumerate
from time import sleep


def test(req):
    class Sn(Thread):
        def run(self) -> None:
            print(11111)
            sleep(10)
            print(22222)


    s = Sn()
    s.name = 'ali'
    print(s.getName())
    s.start()
    print('----')
    print(active_count())
    return HttpResponse(111)

def test2(reeq):
    for thread in enumerate():
        print(thread.name)
    return HttpResponse(9999)

urlpatterns = [
    path('test/', test),
    path('test2/', test2),
    path('admin/', admin.site.urls),
]

# include all module's urls
urlpatterns += theater_urlpatterns
urlpatterns += shop_urlpatterns

# include static files urls
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)