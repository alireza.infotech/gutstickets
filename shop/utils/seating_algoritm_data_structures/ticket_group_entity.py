from shop.models import User


class TicketGroupEntity:
    """
    Data structure to store a partition of free seats often is a row and if
    a row hase for example a block seat then the row defines as two partitions
    """
    number_of_ticket: int = 0
    # Distance between current group index from first group index EXP:
    # list: [a,b,c,d,e] --> element_index / list_length for instance
    # a: 1 / 5 = 0.2  b: 2 / 5 = 0.4 ... e: 5 / 5 = 1
    interception_elevation: float = None
    user: User = None
    min_diff_value = 0
    min_diff_partition_index = 0

    def __init__(self, number_of_ticket: int, interception_elevation: float, user: User):
        self.number_of_ticket = number_of_ticket
        self.interception_elevation = interception_elevation
        self.user = user

    def __str__(self):
        return F'{self.interception_elevation} --> {self.number_of_ticket} --> {self.user.fullname} --. {self.min_diff_value}'
