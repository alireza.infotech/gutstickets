from .ticket_group_entity import TicketGroupEntity


class SeatingPartitionEntity:
    """
    Data structure to store a partition of free seats often is a row and if
    a row hase for example a block seat then the row defines as two partitions
    """
    seats: list = []
    # Distance between current row index from first row EXP:
    # list: [a,b,c,d,e] --> element_index / list_length for instance
    # a: 1 / 5 = 0.2  b: 2 / 5 = 0.4 ... e: 5 / 5 = 1
    interception_elevation: float = None
    remaining_free_seats: int = 0
    predict_available_free_seats: int = 0
    ticket_group_list = None

    def __init__(self, seats: [], interception_elevation: float,
                 remaining_free_seats: int,
                 predict_available_free_seats: int):
        self.seats = seats
        self.remaining_free_seats = remaining_free_seats
        self.interception_elevation = interception_elevation
        self.predict_available_free_seats = predict_available_free_seats
        self.ticket_group_list = []

    def __str__(self):
        return F'{self.interception_elevation} {self.seats}'
