from utils.chain_of_responsibility import AbstractHandler
from typing import Any
from django.db import transaction
from shop.utils.seating_algoritm_data_structures.partition_partition_entity import SeatingPartitionEntity
from shop.models import BookedEvent


class SeatingResultCommitHandler(AbstractHandler):

    # all allocated seats must be inserted or rollback
    @transaction.atomic
    def handle(self, request: Any) -> str:

        allocated_seats_partitions: [SeatingPartitionEntity] = request.get('allocated_seats_partitions')
        event = request.get('event')
        # print(allocated_seats_partitions)
        # In case another
        BookedEvent.objects.filter(event=event).delete()
        direction = True  # True indicates LTR and vice versa
        try:
            for allocated_seats_partition in allocated_seats_partitions:
                j = 0
                for ticket_group in allocated_seats_partition.ticket_group_list:
                    end = j + ticket_group.get('slice', 0)
                    for i in range(j, end):
                        j += 1
                        booked_event = BookedEvent(
                            event=event,
                            user=ticket_group.get('ticket_group_entity').user,
                            seat=allocated_seats_partition.seats[i] if direction else
                            allocated_seats_partition.seats.reverse()[i]
                        )
                        booked_event.save()
                direction = not direction
        except Exception as r:
            print(r)
        return super().handle(request)
