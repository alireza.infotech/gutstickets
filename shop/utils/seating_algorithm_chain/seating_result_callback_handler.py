from utils.chain_of_responsibility import AbstractHandler
from typing import Any, Optional


class SeatingCallbackHandler(AbstractHandler):
    def handle(self, request: Any) -> str:
        return super().handle(request)
