from utils.chain_of_responsibility import AbstractHandler
from typing import Any
import pydash as _
import bisect
import sys
from shop.utils.seating_algoritm_data_structures.partition_partition_entity import SeatingPartitionEntity
from shop.utils.seating_algoritm_data_structures.ticket_group_entity import TicketGroupEntity


def sort_ticket_group_depend_min_difference_with_free_seats(partition_entities, ticket_group_entities):
    """
    fixme: add readme here
    this values used to decide which user must gets seat sooner.
    :return:
    """
    for ticket_group_entity in ticket_group_entities:
        min_diff_value = sys.maxsize
        min_diff_partition_index = -1
        min_interception_diff = 1
        for key, partition_entity in enumerate(partition_entities):
            diff = partition_entity.predict_available_free_seats - ticket_group_entity.number_of_ticket
            interception_diff = abs(ticket_group_entity.interception_elevation - partition_entity.interception_elevation)
            if (0 <= diff < min_diff_value and partition_entity.predict_available_free_seats != 0) or (diff == min_diff_value and interception_diff < min_interception_diff):
                min_diff_value = diff
                min_diff_partition_index = key
                min_interception_diff = interception_diff

        ticket_group_entity.min_diff_value = min_diff_value
        ticket_group_entity.min_diff_partition_index = min_diff_partition_index
        tmp = {
            'ticket_group_entity': ticket_group_entity,
            'slice': ticket_group_entity.number_of_ticket
        }
        if ticket_group_entity.min_diff_partition_index % 2 == 0:
            partition_entities[min_diff_partition_index].ticket_group_list.append(tmp)
        else:
            partition_entities[min_diff_partition_index].ticket_group_list.insert(0, tmp)
        partition_entities[min_diff_partition_index].predict_available_free_seats -= ticket_group_entity.number_of_ticket


def add_group_ticket_to_a_partition(partition_entities: [SeatingPartitionEntity],
                                    group_ticket_entity: TicketGroupEntity):
    partition_entity: SeatingPartitionEntity = partition_entities[group_ticket_entity.min_diff_partition_index]
    try:
        if (partition_entity.remaining_free_seats - group_ticket_entity.number_of_ticket) >= 0:
            # To recognize add group_ticket too first or end of list
            partition_entity.remaining_free_seats -= group_ticket_entity.number_of_ticket
        else:
            # Fixme: must bee implemented
            pass
    except Exception as e:
        print(e)


class SeatingAlgorithmHandler(AbstractHandler):

    def handle(self, request: Any) -> str:
        partition_entities = request.get('unallocated_seats_partitions_entities')
        ticket_group_entities: list = request.get('ticket_group_entities')
        sort_ticket_group_depend_min_difference_with_free_seats(
            partition_entities,
            ticket_group_entities)

        return super().handle({
            'allocated_seats_partitions': partition_entities,
            'event': request.get('event')
        })