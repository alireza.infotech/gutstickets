from utils.chain_of_responsibility import AbstractHandler
from typing import Any
from theater.models import Seat, Row, Rank
from shop.models import Event
from shop.utils.seating_algoritm_data_structures.partition_partition_entity import SeatingPartitionEntity
from shop.utils.seating_algoritm_data_structures.ticket_group_entity import TicketGroupEntity
import pydash as _


def get_partitions(event: Event, rank: Rank):
    row_ids = Seat.objects\
        .filter(row__section__theater=event.theater, rank=rank)\
        .values('row_id').distinct()

    rows = Row.objects.filter(pk__in=row_ids).order_by('section__order_in_theater', 'order_in_section')
    return [
        SeatingPartitionEntity(
            interception_elevation=key/rows.count(),
            # put all seats in a row because we dont support block seats
            seats=cur.row_seats.all().order_by('order_in_row'),
            remaining_free_seats=cur.row_seats.count(),
            predict_available_free_seats=cur.row_seats.count())
        for key, cur in enumerate(rows, 1)
    ]


def get_ticket_group(ticket_group: list):
    # Calculate interception elevation to get how far is this element from first of list
    # with a number between 0 to 1, this will be used to compare with row list element
    # fixme : add read me here for more info
    return sorted([TicketGroupEntity(cur.get('number_of_ticket'), index/len(ticket_group), cur.get('user'))
                   for index, cur in enumerate(ticket_group, 1)], key=lambda index: index.number_of_ticket, reverse=True)


class LoadTheaterMapHandler(AbstractHandler):
    def handle(self, query_data: Any) -> str:
        # making partitions to support block seats, but for now we dont support
        # block seats
        event = query_data.get('event')
        partitions = get_partitions(event, query_data.get('rank'))
        ticket_group = get_ticket_group(query_data.get('ticket_group'))

        # will be send to next stage -> seating algorithm
        return super().handle({
            'unallocated_seats_partitions_entities': partitions,
            'ticket_group_entities': ticket_group,
            'event': event
        })
