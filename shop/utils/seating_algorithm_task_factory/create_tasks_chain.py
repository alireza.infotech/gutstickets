from ..seating_algorithm_chain.seating_algorithm_handler import SeatingAlgorithmHandler
from ..seating_algorithm_chain.seating_result_commit_handler import SeatingResultCommitHandler
from ..seating_algorithm_chain.seating_result_callback_handler import SeatingCallbackHandler
from ..seating_algorithm_chain.seating_algorithm_evaluation_handler import SeatingAlgorithmEvaluationHandler
from ..seating_algorithm_chain.load_theater_map_handler import LoadTheaterMapHandler


def create_seating_algorithm_chain():
    load_theater_map_handler = LoadTheaterMapHandler()
    seating_algorithm = SeatingAlgorithmHandler()
    seating_algorithm_evaluation = SeatingAlgorithmEvaluationHandler()
    seating_result_commit = SeatingResultCommitHandler()
    seating_result_callback = SeatingCallbackHandler()

    load_theater_map_handler\
        .set_next(seating_algorithm)\
        .set_next(seating_algorithm_evaluation)\
        .set_next(seating_result_commit)\
        .set_next(seating_result_callback)

    return load_theater_map_handler
