from .create_tasks_chain import create_seating_algorithm_chain
from .thread_pool import ThreadPool
from django.conf import settings

pool = ThreadPool(max_workers=settings.THREAD_POOL_MAX_WORKER)


def create_non_blocking_seating_algorithm_task(task_data):
    chain = create_seating_algorithm_chain()
    pool.submit(chain.handle, task_data)
