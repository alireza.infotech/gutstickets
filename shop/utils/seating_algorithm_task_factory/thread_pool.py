from concurrent.futures.thread import ThreadPoolExecutor
from threading import Event


class ThreadPool(ThreadPoolExecutor):
    def __init__(self, **args):
        super().__init__(**args)

    def __del__(self):
        self.shutdown()

