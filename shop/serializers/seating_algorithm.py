from rest_framework import serializers
from theater.models import Rank
from ..models import User, Event


class TicketGroupSerializer(serializers.Serializer):
    number_of_ticket = serializers.IntegerField(min_value=1)
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())


class SeatingAlgorithmSerializer(serializers.Serializer):
    rank = serializers.PrimaryKeyRelatedField(required=True, queryset=Rank.objects.all())
    ticket_group = serializers.ListField(
        child=TicketGroupSerializer(required=True, many=False),
        min_length=1
    )
    event = serializers.PrimaryKeyRelatedField(required=True, queryset=Event.objects.all())

