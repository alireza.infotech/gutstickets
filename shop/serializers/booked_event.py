from ..models import BookedEvent
from rest_framework.serializers import ModelSerializer
from theater.serializers import SeatSerializer


class BookedEventSerializer(ModelSerializer):
    seat = SeatSerializer(many=False, read_only=True)
    class Meta:
        model = BookedEvent
        fields = '__all__'
