from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.request import Request
from utils import create_response_schema
from shop.serializers import SeatingAlgorithmSerializer
from shop.utils import create_non_blocking_seating_algorithm_task

from rest_framework import status as status_codes


class SeatingAlgorithmAPIView(APIView):
    """
    """

    def post(self, request: Request, format=None):
        """
        Return a task result if query was sync or a task creation success/failure
        if query was async
        """
        seating_algorithm_serializer = SeatingAlgorithmSerializer(data=request.data)
        response: Response = None

        try:
            if not seating_algorithm_serializer.is_valid():
                return create_response_schema(
                    success=False,
                    status=status_codes.HTTP_406_NOT_ACCEPTABLE,
                    data=seating_algorithm_serializer.errors)

            validated_data = seating_algorithm_serializer.validated_data

            create_non_blocking_seating_algorithm_task(validated_data)

            response = create_response_schema(
                success=True,
                status=status_codes.HTTP_201_CREATED,
                data={
                    'message': 'non-blocking task for algorithm created'
                }
            )

            return response
        except Exception as e:
            response = create_response_schema(
                False,
                status_codes.HTTP_500_INTERNAL_SERVER_ERROR,
                data={
                    'message': str(e)
                })

        return response