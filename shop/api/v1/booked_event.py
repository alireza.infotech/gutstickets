from rest_framework.views import APIView
from rest_framework import status
from utils import create_response_schema

from ...models import (
    Event
)

from theater.models import Theater
from theater.serializers import TheaterSerializer

from shop.serializers import BookedEventSerializer


class BookedEventAPIView(APIView):

    def get(self, request, event_id):

        if not Event.objects.filter(pk=event_id).exists():
            return create_response_schema(
                data={
                    'message': 'please provide a valid event id',
                    'valid_event_ids': [cur['id'] for cur in Event.objects.all().values('id')]
                },
                success=False,
                status=status.HTTP_406_NOT_ACCEPTABLE
            )
        booked_event = Event.objects.get(pk=event_id).booked.all()
        theater = Event.objects.get(pk=event_id).theater
        return create_response_schema(
            data={
                'theater': TheaterSerializer(instance=theater, many=False).data,
                'booked_events': BookedEventSerializer(instance=booked_event, many=True).data
            },
            success=True,
            status=status.HTTP_200_OK
        )
