from django.db import models
from .user import User
from .event import Event
from theater.models import Seat


class BookedEvent(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='user_booked_events')
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='booked')
    seat = models.ForeignKey(Seat, on_delete=models.SET_NULL, null=True, related_name='booked_events')
    booking_time = models.DateTimeField(auto_now_add=True)
