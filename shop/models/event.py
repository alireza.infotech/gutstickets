from django.db import models
from theater.models import Theater


class Event(models.Model):
    name = models.CharField(max_length=255, blank=False)
    theater = models.ForeignKey(Theater, on_delete=models.CASCADE, related_name='theater_events')

    def __str__(self):
        return self.name
