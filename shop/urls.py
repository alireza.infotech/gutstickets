from django.urls import path, include
from shop.api.v1.seating_algorithm_trigger import SeatingAlgorithmAPIView
from shop.api.v1.event import EventViewSet
from shop.api.v1.user import UserViewSet
from shop.api.v1.booked_event import BookedEventAPIView


from utils.optional_slash_router import OptionalSlashRouter

router = OptionalSlashRouter()


router.register('api/v1/event',
                EventViewSet,
                'events')

router.register('api/v1/user',
                UserViewSet,
                'users')

urlpatterns = [
    path('api/v1/seating-algorithm', SeatingAlgorithmAPIView.as_view()),
    path('api/v1/booked-event/<int:event_id>', BookedEventAPIView.as_view()),
    path('', include(router.urls))
]
