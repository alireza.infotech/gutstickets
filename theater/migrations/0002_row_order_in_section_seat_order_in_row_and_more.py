# Generated by Django 4.0 on 2021-12-24 16:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('theater', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='row',
            name='order_in_section',
            field=models.FloatField(default=1, unique=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='seat',
            name='order_in_row',
            field=models.FloatField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='section',
            name='order_in_theater',
            field=models.FloatField(default=1),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='row',
            unique_together={('order_in_section', 'section')},
        ),
        migrations.AlterUniqueTogether(
            name='seat',
            unique_together={('order_in_row', 'row')},
        ),
        migrations.AlterUniqueTogether(
            name='section',
            unique_together={('order_in_theater', 'theater')},
        ),
    ]
