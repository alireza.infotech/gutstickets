from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.viewsets import ModelViewSet
from ...models import (
    SeatProperty,
    Seat
)
from ...serializers import (
    SeatPropertySerializer,
    SeatSerializer
)


class SeatViewSet(ModelViewSet):
    queryset = Seat.objects.all()
    serializer_class = SeatSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['name']
    lookup_field = 'pk'


class SeatPropertyViewSet(ModelViewSet):
    queryset = SeatProperty.objects.all()
    serializer_class = SeatPropertySerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['name']
    lookup_field = 'pk'
