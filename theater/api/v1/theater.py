from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.viewsets import ModelViewSet
from ...models import (
    Theater
)
from ...serializers import (
    TheaterSerializer
)


class TheaterViewSet(ModelViewSet):
    queryset = Theater.objects.all()
    serializer_class = TheaterSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['name']
    lookup_field = 'pk'
