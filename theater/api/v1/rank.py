from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.viewsets import ModelViewSet
from ...models import (
    Rank
)
from ...serializers import (
    RankSerializer
)


class RankViewSet(ModelViewSet):
    queryset = Rank.objects.all()
    serializer_class = RankSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['name']
    lookup_field = 'pk'
