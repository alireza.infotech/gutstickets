from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.viewsets import ModelViewSet
from ...models import (
    Section,
    Row
)
from ...serializers import (
    SectionSerializer,
    RowSerializer
)


class SectionViewSet(ModelViewSet):
    queryset = Section.objects.all()
    serializer_class = SectionSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['name']
    lookup_field = 'pk'


class RowViewSet(ModelViewSet):
    queryset = Row.objects.all()
    serializer_class = RowSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['name']
    lookup_field = 'pk'
