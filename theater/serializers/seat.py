from ..models import Seat, SeatProperty, Rank
from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField


class SeatSerializer(ModelSerializer):
    rank = PrimaryKeyRelatedField(required=True, queryset=Rank.objects.all())

    class Meta:
        model = Seat
        fields = '__all__'


class SeatPropertySerializer(ModelSerializer):

    class Meta:
        model = SeatProperty
        fields = '__all__'
