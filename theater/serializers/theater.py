from ..models import Theater
from rest_framework.serializers import ModelSerializer
from .section import SectionSerializer


class TheaterSerializer(ModelSerializer):
    sections = SectionSerializer(many=True, read_only=True, source='theater_sections')

    class Meta:
        model = Theater
        fields = '__all__'
