from ..models import Rank
from rest_framework.serializers import ModelSerializer


class RankSerializer(ModelSerializer):

    class Meta:
        model = Rank
        fields = '__all__'
