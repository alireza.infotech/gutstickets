from ..models import Section, Row, Seat
from rest_framework.serializers import ModelSerializer, ListSerializer, PrimaryKeyRelatedField
from .seat import SeatSerializer


class RowSerializer(ModelSerializer):
    seats = SeatSerializer(many=True, read_only=True, source='row_seats')

    class Meta:
        model = Row
        fields = '__all__'


class SectionSerializer(ModelSerializer):
    rows = RowSerializer(many=True, read_only=True, source='section_rows')

    class Meta:
        model = Section
        fields = '__all__'

