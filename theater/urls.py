from django.urls import path, include
from theater.api.v1.seat import SeatViewSet, SeatPropertyViewSet
from theater.api.v1.rank import RankViewSet
from theater.api.v1.section import RowViewSet, SectionViewSet
from theater.api.v1.theater import TheaterViewSet


from utils import OptionalSlashRouter

router = OptionalSlashRouter()

router.register('api/v1/seat',
                SeatViewSet,
                'seats')

router.register('api/v1/seat-property',
                SeatPropertyViewSet,
                'seats-properties')

router.register('api/v1/theater',
                TheaterViewSet,
                'theaters')

router.register('api/v1/section',
                SectionViewSet,
                'sections')

router.register('api/v1/row',
                RowViewSet,
                'rows')

router.register('api/v1/rank',
                RankViewSet,
                'ranks')

urlpatterns = [
    path('', include(router.urls))
]
