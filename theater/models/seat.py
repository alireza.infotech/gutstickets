from django.db import models
from .rank import Rank
from .section import Row


class Seat(models.Model):
    """
    Stores Seat's data
    """
    name = models.CharField(max_length=200, blank=False)
    # order in row defines where placed the seat in a specific row, it must be
    # unique in a row because two seats cant be in a same place
    # lower order indicates most left seat and vice versa
    # help: if you want place a seat between two other seat, you can just set always
    # the average of immediate left and immediate right seat
    # example: right order 10 left order 11 new seat order would be 10.5 :)
    order_in_row = models.FloatField(blank=False)
    rank = models.ForeignKey(Rank, on_delete=models.SET_NULL, null=True, related_name='rank_seats')
    row = models.ForeignKey(Row, on_delete=models.CASCADE, related_name='row_seats')

    class Meta:
        unique_together = ('order_in_row', 'row')

    def __str__(self):
        return self.name


class SeatProperty(models.Model):
    """
    Stores SeatProperty's data
    """
    name = models.CharField(max_length=200, blank=False)
    seats = models.ManyToManyField(Seat, related_name='seat_properties')

    def __str__(self):
        return self.name

