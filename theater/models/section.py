from django.db import models
from .theater import Theater


class Section(models.Model):
    """
    Stores Section's data
    """
    name = models.CharField(max_length=200, blank=False)
    # order in theater defines where placed the section in a specific theater, it must be
    # unique in a theater because two section cant be in a same place!
    # lower order indicates front most section and vice versa
    # help: if you want place a section between two other section, you can just set always
    # the average of immediate upper and lower section
    # example: upper order 10, lower order 11, new row order would be 10.5 :)
    order_in_theater = models.FloatField(blank=False)
    theater = models.ForeignKey(Theater, on_delete=models.CASCADE, related_name='theater_sections')

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('order_in_theater', 'theater')


class Row(models.Model):
    """
    Stores Row's data
    """
    name = models.CharField(max_length=200, blank=False)
    # order in section defines where placed the row in a specific section, it must be unique
    # because two row cant be in a same place!
    # lower order indicates front most row and vice versa
    # help: if you want place a row between two other rows, you can just set always
    # the average of immediate upper and lower rows
    # example: upper order 10 lower order 11 new row order would be 10.5 :)
    order_in_section = models.FloatField(blank=False, unique=True)
    section = models.ForeignKey(Section, on_delete=models.CASCADE, related_name='section_rows')

    class Meta:
        unique_together = ('order_in_section', 'section')

    def __str__(self):
        return self.name

