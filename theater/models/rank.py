from django.db import models


class Rank(models.Model):
    """
    Stores Rank's data
    """
    name = models.CharField(max_length=200, blank=False)

    def __str__(self):
        return self.name




