#!/bin/bash
printenv
python3 manage.py collectstatic --no-input
python3 manage.py migrate
gunicorn --workers 3 --bind 0.0.0.0:8003 gutstickets.wsgi
