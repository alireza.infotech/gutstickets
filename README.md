
# GUTS Tickets


As you know, to build any algorithm, the following processes must be performed:
- Recognize the problem: How to allocate seats to the list of users in a rank so that the following priorities are met:
  
  In order : 
  1) Each ticket group has the least Break - Break: creating a gap between the users of a ticket group - to be created.
  2) Users who bought tickets earlier will get more front seats.
  3) Users get seats with special features


- The algorithm is implemented with the design of suitable patterns with the least execution time and the least RAM memory.

- Evaluate the algorithm to determine how much the problem needs have been answered according to the specified priorities.

# My Approach 
To solve this task, I decided to use the Django framework as well as the design of the Chain of Responsibility pattern.

## Modules
  - Theater : stores theater map
    - Models : 
      - Theater : each theater has multiple section
      - Section : each section has multiple rows
      - Row: each has multiple seat
      - Seat
      - Rank: each seat is in just one rank
  - Shop: stores ticketing data and runs ticketing algorithm
    - Models:
      - Event: each event is for single theater
      - User: Head of group ticket users
      - bookedEvent: stores (event, seat, user)

  - Loading data is specific order
    ```
    Theater > Section (order_in_theater field) > Row (order_in_section field) > Seat (order_in_row field)
    ```

    To be illustrated : 
    ![App Screenshot](https://python.lamasoo.com/media/la_users/Screen_Shot_1400-10-15_at_02.25.05.png)

  ## Design pattern (Chain Of Responsibility ):
  
    1) Load Theater map
    2) Run Seating algorithm
    3) Evaluate result score ( Not Implemented because of time limitation) 
    4) store data in data base : Transactional
    5) Callbacks when task finished ( No callback provided ) 

  ## Task management
  I decided to use a Thread Pool for this because they have more control and will also cost less.
    
  ## DataBase
  About the database, I prefer Postgres but for ease of use in assessment project I selected sqlite
  ## Data Structure
  By partitioning the space, you can also handle the block seats
   - SeatingPartitionEntity
   - TicketGroupEntity 

  ## Algorithm 
  First, I tried to minimize the number of Breaks, so I decided to first give seats to users whose tickets exceed the length of our largest partition, because it is assumed that those who group The bigger they are, the more Breaks they cause.
Then, by obtaining the difference between each ticket-group and the number of available empty spaces, I will first give seats to those who complete the partition.
 For example, if there are three partitions and one has 3 empty spaces, one has 2 empty spaces and the other has 4 empty spaces, then the priority is with a place that has 3 empty spaces because it is more complete.

Then in the next step, I tried to pay attention to the priority of buying a ticket, so two things have been done:
First, in the sorting process, if there were two groups completing a partition, the priority was given to the group that bought the ticket earlier. Also, if there were two partitions that were completed with a group of tickets, a seat was allocated to the user, which is more appropriate according to the time of purchasing the ticket. I used the following method to do this:

```
group_tickets = [1, 3, 4, 4, 5, 1, 2, 4]

# interception_elevation -> group_ticket_index / group_tickets_length

group_ticket_interception_elevation = [0.12, 0.25, .... , 0.87, 1]

partion_list = [first_row,second_row,last_row]

partion_list_interception_elevation = [0.333, 0.666, 1]
```

For example group with 5 ticket in algorithm gets second row seats because:

```
first_row --> is full
second_row --> | 0.66 - 0.62 | = 0.04
third_row --> | 1 - 0.62 | = 0.38

then
0.04 < 0.38 

so
gets econd row seats
```


    

## Deployment

## Server 
clone projcet 

```bash
git clone https://gitlab.com/alireza.infotech/gutstickets.git
```

#### Manual:

Install virtual environment on your machine and create a viertuan env with python 3.6+

```bash
  virtualenv env

  source env/bin/active
```

Install dependencies : 
```bash
  pip3 install -r req

```
Run application with port 8003

```bash
  python3 manage.py runserver:8003 
```


#### Docker:

Install docker and in root folder run 

```bash
docker-compose -f docker-compose-staging.yml up
```

## Client 
clone projcet 

```bash
git clone https://gitlab.com/alireza.infotech/gutstickets-ui.git
```

#### Manual:

Install nodejs and npm using this link https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

Install dependencies
```bash
npm i 
```

Run the project

```bash
npm run serve 
```

## Test

Code doesn't have any tests, I know :) but it's because of time limitation
, but I provided a postman collection to test APIs
It's in code directory


## Authors

- [@AlirezaKhosravian](https://www.linkedin.com/in/alirezainfotech/)

